const canvas = document.getElementById('canvasLand');
const context = canvas.getContext('2d')

class Random{
    static get(inicio, final){
        return Math.floor(Math.random() * final) + inicio; 
    }
}


class Food{
    constructor(x,y){
        this.x = x
        this.y = y
        this.width = 10
        this.height = 10
    }

    draw(){
        context.fillRect(this.x, this.y, 10,10)
    }

    static generateFood(){
        return new Food(Random.get(0,500), Random.get(0,500));
    }
}

class Square{
    constructor(x,y){
        this.x = x
        this.y = y
        this.direction = "right"
        this.back = null
        this.width = 10
        this.height = 10
    }

    draw(){
        context.fillRect(this.x,this.y,this.width, this.height)
        if(this.hasBack()){
            this.back.draw()
        }
     }
    
    add(){
        if(this.hasBack()) return this.back.add();
        this.back = new Square(this.x, this.y)
    }
 
    hasBack(){
        return this.back !== null
    }

    copy(){
        if(this.hasBack()){
            this.back.copy()

            this.back.x = this.x
            this.back.y = this.y
        }
    }
    
    rigth(){
        this.copy()
        this.x += 10
    }

    left(){
        this.copy()
        this.x -= 10
    }

    up(){
        this.copy()
        this.y -= 10
    }

    down(){
        this.copy()
        this.y += 10
    }

    hit(head, second = false){
        if(this === head && !this.hasBack()) return false
        if(this === head) return this.back.hit(head, true)
        if(second && !this.hasBack()) return false
        if(second)  return this.back.hit(head)


        if(this.hasBack()){
            return squareHit(this,head) ||  this.back.hit(head)
        }

        return squareHit(this,head)
    }
}

class Snake{
    constructor(){
        this.head = new Square(250,250)
        this.draw()
        this.head.add()
    }
    draw(){
       this.head.draw()
    }

    eat(){
        this.head.add()
    }

    dead(){
        return this.head.hit(this.head)
    }
    //Move
    down(){
        this.direction = "down"
    }

    up(){
        this.direction = "up"
    }

    rigth(){
        this.direction = "right"
    }

    left(){
        this.direction = "left"
    }

    move(){
        if(this.direction === "up") return this.head.up()
        if(this.direction === "down") return this.head.down()
        if(this.direction === "left") return this.head.left()
        if(this.direction === "right") return this.head.rigth()
    }
}

const snake = new Snake()
let foods = []


window.addEventListener("keydown", function(e){
    if(e.keyCode === 40) return snake.down();
    if(e.keyCode === 39) return snake.rigth();
    if(e.keyCode === 37) return snake.left();
    if(e.keyCode === 38) return snake.up();
})

const gameReady = setInterval(function(){
    snake.move()
    context.clearRect(0,0,canvas.width,canvas.height)
    snake.draw()
    drawFood()

    if(snake.dead){
        console.log("end");
        window.clearInterval(gameReady)
    }
},1000 / 10)


setInterval(function(){
    const food = Food.generateFood()
    foods.push(food)

    setTimeout(function(){
        removeFromFoods(food)
    },10000)
},4000);

function drawFood(){
    for(const i in foods){
        const food = foods[i]

        if(typeof food !== "undefined"){
            food.draw()
            if(hit(food, snake.head)){
               snake.eat()
                removeFromFoods(food)
           }
        }
    }
}

function removeFromFoods(food){
    foods = foods.filter(function(f){
        return food !== f
    })
}


//Colitions
function squareHit(squareOne, squareTwo){
    return squareOne.x == squareTwo.x && squareOne.y == squareTwo.y
}


function hit(objectA, objectB)
{
    let hit = false;

if(objectB.x + objectB.width >= objectA.x && objectB.x < objectA.x + objectA.width){
    if(objectB.y + objectB.height >= objectA.y && objectB.y  < objectA.y + objectA.height){
        hit = true;
    }
}

if(objectB.x <= objectA.x && objectB.x + objectB.width >= objectA.x + objectA.width){
    if(objectB.y <= objectA.y && objectB.y + objectB.height >= objectA.y + objectA.height){
        hit = true;
    }
}


if(objectA.x <= objectB.x && objectA.x + objectA.width >= objectB.x + objectB.width){
    if(objectA.y <= objectB.y && objectA.y + objectA.height >= objectB.y + objectB.height   ){
        hit = true;
    }
}

    return hit = true;
}